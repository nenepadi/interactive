var r = 0;
var g = 100;
var b = 255;

function setup() {
    createCanvas(600, 400);
}

function draw() {
    r = map(mouseX, 0, 600, 0, 255);
    b = map(mouseX, 0, 600, 255, 0);
    col = map(mouseY, 0, 400, 100, 255);
    background(r, g, b);

    fill(250, 118, 222);
    ellipse(mouseX, 200, 64, 64);
}