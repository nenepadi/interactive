var spot = {
    x: 100,
    y: 50
};

var col = {
    r: 255,
    g: 0,
    b: 0
};

function setup() {
    createCanvas(600, 400);
    background(0);
}

function draw() {
    spot.x = random(0, 600);
    spot.y = random(0, 400);
    col.r = random(0, 255);
    col.g = random(100, 200);
    col.b = random(0, 150);

    noStroke();
    fill(col.r, col.g, col.b, 80);
    ellipse(spot.x, spot.y, 24, 24);
}