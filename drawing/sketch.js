function setup() {
    createCanvas(640, 360);
}

function draw() {
    rect(100, 100, 50, 30);
    line(12, 34, 50, 100);
    arc(67, 150, 100, 60, 400, false);
}