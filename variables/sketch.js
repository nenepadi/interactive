// variables - using p5.js variables ..... 
// function setup() {
//     createCanvas(600, 400);
//     background(250, 250, 100);
// }

// function draw() {
//     noStroke();
//     fill(250, 200, 200, 50);
//     ellipse(mouseX, mouseY, 25, 25);
// }

// function mousePressed() {
//     background(250, 250, 100);
// }

// variables - creating your own variables ...

var circleX = 0;

function setup() {
    createCanvas(600, 400);
}

function draw() {
    background(250, 250, 100);

    fill(250, 200, 200);
    ellipse(circleX, 200, 80, 80);
    circleX = circleX + 1;
}