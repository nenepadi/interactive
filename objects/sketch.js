var circle = {
    x: 0,
    y: 200,
    diameter: 50,
    color: {
        r: 255,
        g: 255,
        b: 255
    }
};

var color = {
    r: 0,
    g: 0,
    b: 0
};

function setup() {
    createCanvas(600, 400);
}

function draw() {
    background(218, 160, 221);

    circle.color.b -= 1;
    circle.color.r -= 1;
    circle.color.g -= 2;

    fill(circle.color.r, circle.color.g, circle.color.b);
    ellipse(circle.x, circle.y, circle.diameter, circle.diameter);
    circle.x += 1;
}